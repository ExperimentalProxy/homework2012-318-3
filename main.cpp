#include <stdio.h>
#include <iostream>
#include <fstream>
#include "list"
#include "queue"

using namespace std;

float *AAT;
float *RAT;
float *slacks;
float *delay;
int *ivert;

list <float> *vrtx;
list <float> *xtrv;

int nofv=0;

int main() {
	// количество вершин nofv - number of vertex
	float heap;
	fstream file;
	file.open("graph.nodes",fstream::in);
	while (file.good()) {
		file>>heap;
		nofv++;
	}
	file.close();
	nofv--;
	// выделяем память
	slacks = new float[nofv];

	AAT = new float[nofv];
	for (int i=0;i<nofv;i++) {
		AAT[i]=0;
	}

	int inpr;
	float wr;
	RAT = new float[nofv];
	for (int i=0;i<nofv;i++) {
		RAT[i]=0;
	}
	file.open("graph.aux",fstream::in);
	while (file.good()) {
		file>>inpr;
		file>>wr;
		RAT[inpr]=wr;
	}
	file.close();



	// заполнение массива вершин и их задержек
	delay = new float[nofv];
	file.open("graph.nodes",fstream::in);
	for (int i=0;i<nofv;i++) {
		file>>delay[i];

	}
	file.close();

	ivert = new int[nofv];// массив, определящющий вершину)
	for (int i=0;i<nofv;i++) {
		ivert[i]=-1;
	} 

	// заполнение списка смежности и весов 
	int inp,outp;
	float weight;

	vrtx=new list <float> [nofv];
	xtrv=new list <float> [nofv];
	file.open("graph.nets",fstream::in);

	while (file.good()) {
		file>>inp;
		if (!file.good()) break;
		if (ivert[inp]==2) ivert[inp]=0;
		else if (ivert[inp]==0);
		else ivert[inp]=1;//1 если есть выходящий
		file>>outp;
		if (ivert[outp]==1) ivert[outp]=0;//0 если есть и вход и выход
		else if (ivert[outp]==0);
		else ivert[outp]=2;
		
		file>>weight;
		if (!file.good()) break;
		vrtx[inp].push_back(outp);
		vrtx[inp].push_back(weight);
		xtrv[outp].push_back(inp);
		xtrv[outp].push_back(weight);
		
	}
	file.close();	

	
	float maxw;
	for (int i=0;i<nofv;i++) {
		if (ivert[i]==2) {
			continue;
		} 
		for(list <float>::iterator  j = vrtx[i].begin();  j != vrtx[i].end(); j++) {
			int  value =(float) *j;
			j++;
			maxw=AAT[i]+*j+delay[value];
			if (AAT[value]<maxw) {
			AAT[value]=maxw;
			}

		}
		
	}

	//MAIN CIRCLE FOR RAT

	for (int i=nofv-1;i>=0;i--) {
		if (ivert[i]==1) {
			continue;
		} 
		for(list <float>::iterator  j = xtrv[i].begin();  j != xtrv[i].end(); j++) {
			int  value =(float) *j;
			j++;
			maxw=RAT[i]-*j-delay[value+1];
			if (RAT[value]<maxw) {
			RAT[value]=maxw;
			}

		}
		
	}
	
	int flag=0;
	for (int i=0;i<nofv;i++) {
		slacks[i]=RAT[i]-AAT[i];
		if (slacks[i]<0) flag=1;
	}

	file.open("graph.slacks",fstream::out);
	for (int i=0;i<nofv;i++) {
		file<<slacks[i]<<'\n';
	}
	file.close();

	file.open("graph.result",fstream::out);
	file<<flag<<'\n';
	if (flag==0) {
		file.close();	
	} else {
		for (int i=0;i<nofv;i++) {
			if (slacks[i]<0) file<<i<<" ";
		}
		file<<'\n';
		file.close();
	}
	
delete[]AAT;
delete[]RAT;
delete[]slacks;
delete[]delay;
delete[]ivert;

delete[]vrtx;
delete[]xtrv;

return 0;
}
